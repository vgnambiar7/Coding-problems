with open('V:\Python Problems\Adevnt problem inputs\Advent_Depth_2.txt') as data:
    lines = data.readlines()
    print(lines)
    # lines = [line.split() for line in lines]

print(lines)


def find_distance():
    forward_count = 0
    depth_count = 0
    for i in range(len(lines)):
        x = lines[i].split(' ')
        if x[0] == 'forward':
            forward_count += int(x[1])
            # print("F count",forward_count)
        elif x[0] == 'down':
            depth_count += int(x[1])
            # print("D count",depth_count)
        else:
            depth_count -= int(x[1])
    print("Output", forward_count * depth_count)


find_distance()
