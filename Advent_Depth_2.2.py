with open('V:\Python Problems\Adevnt problem inputs\Advent_Depth_2.txt') as data:
    lines = data.readlines()
    print(lines)
    # lines = [line.split() for line in lines]


def find_distance():
    forward_count = 0
    depth_count = 0
    aim_count = 0

    for i in range(len(lines)):
        x = lines[i].split(' ')
        if x[0] == 'forward':
            forward_count += int(x[1])
            depth_count += aim_count * int(x[1])
            # print("Forward count", forward_count)
            # print("Depth count", depth_count)
            # print("Aim count", aim_count)
            # print("--------", i, "---------")
        elif x[0] == 'down':
            aim_count += int(x[1])
            # print("Forward count", forward_count)
            # print("Depth count", depth_count)
            # print("Aim count", aim_count)
            # print("--------", i, "---------")
        elif x[0] == 'up':
            aim_count -= int(x[1])
            # print("Forward count", forward_count)
            # print("Depth count", depth_count)
            # print("Aim count", aim_count)
            # print("--------", i, "---------")

    print("O/P", forward_count * depth_count)


find_distance()
