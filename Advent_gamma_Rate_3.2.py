def main():
    with open(r'C:\Users\Vaishak\Downloads\Code\GitLab\Coding_problems\Advent\input_3.1.txt') as data:
        inputn = data.readlines()
        input_lst = [line.strip() for line in inputn]
    o2_gen = o2_generator_rating(input_lst)
    print(o2_gen)
    co2_scr = co2_scrubber_rating(input_lst)
    print(co2_scr)
    life_support_rating = o2_gen * co2_scr
    print("**the life support rating of the submarine is **", life_support_rating)


def convert_to_decimal(val):
    return int(val, 2)


def o2_generator_rating(input_lst):
    i = 0
    while len(input_lst) != 1:
        zero_array = []
        one_array = []
        for j in range(len(input_lst)):
            item = input_lst[j][i]
            print("item", item)
            if item == '0':
                zero_array.append(input_lst[j])
            elif item == '1':
                one_array.append(input_lst[j])
        if len(zero_array) > len(one_array):
            input_lst = zero_array
        elif len(zero_array) < len(one_array):
            input_lst = one_array
        else:
            input_lst = one_array
        i = i + 1
    oxy_gen_rating = convert_to_decimal(input_lst[0])
    print("The oxygen generation rating is", oxy_gen_rating)
    return oxy_gen_rating


def co2_scrubber_rating(input_lst):
    i = 0
    while len(input_lst) != 1:
        zero_array = []
        one_array = []
        for j in range(len(input_lst)):
            item = input_lst[j][i]
            print("item", item)
            if item == '0':
                zero_array.append(input_lst[j])
            elif item == '1':
                one_array.append(input_lst[j])
        if len(zero_array) < len(one_array):
            input_lst = zero_array
        elif len(zero_array) > len(one_array):
            input_lst = one_array
        else:
            input_lst = zero_array
        i = i + 1
    co2_gen_rating = convert_to_decimal(input_lst[0])
    print("The oxygen generation rating is", co2_gen_rating)
    return co2_gen_rating


main()
